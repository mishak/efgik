# ElasticSearch Filebeat Kibana stack

## Getting Started

```sh
# linux: increase map count
sudo sysctl -w vm.max_map_count=262144
# mac and windows
docker-machine ssh default sudo sysctl -w vm.max_map_count=262144

# checkout this repository
git clone https://gitlab.com/mishak/efgik.git
cd efgik

# start elasticsearch, grafana and kibana services
docker-compose up -d elasticsearch grafana kibana

# import via filebeat
cat data.json | docker-compose run --rm filebeat.pipe

# collect logs from running docker containers on host
docker-compose up -d filebeat.docker
```

## Services

* [Elasticsearch](http://localhost:9200)
* [Grafana](http://localhost:5601)
* [Kibana](http://localhost:5601)

## Customizations

### Memory, versions and ports

```sh
# edit .env
vim .env
# recreate service(s)
docker-compose up --build -d elasticsearch grafana kibana
```

### Images

```sh
# edit Dockerfile
vim elasticsearch/Dockerfile
# recreate services from new image
docker-compose up --build -d elasticsearch
```
